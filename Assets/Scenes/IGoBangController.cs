﻿
public interface IGoBangController
{

    void OnEnable(GoBangTest view);

    void OnDisable();

    void OnDestroy();

    void OnWaitStart();

    void OnDownChess(int turnFlag, int row, int col);

    void OnUpdate();

    void OnLeftTimeUp();

    void OnNextTurn();

}
