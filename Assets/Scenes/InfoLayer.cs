﻿using UnityEngine;
using UnityEngine.UI;


public class InfoLayer : MonoBehaviour
{
    private static InfoLayer m_Inst;
    public static InfoLayer Instance
    {
        get { return m_Inst; }
    }

    public Text m_TxtMsg;
    public Button m_BtnOk;

    public InfoLayer()
    {
        if (null == m_Inst)
            m_Inst = this;
        else
            Debug.LogError($"{GetType().Name}: multi Inst");
    }

    void OnDestroy()
    {
        if (m_Inst == this) m_Inst = null;
    }

    void Start()
    {
        m_BtnOk.onClick.AddListener(OnClick_Ok);
    }

    void OnDisable()
    {
        m_TxtMsg.text = "";
    }

    private void OnClick_Ok()
    {
        this.gameObject.SetActive(false);
    }

    public void SetGameKey(string gameKey)
    {
        m_TxtMsg.text = $"我的IP：{SocketHelper.GetIpV4Address()}\n密令：{gameKey}";
    }

}

