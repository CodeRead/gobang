﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultLayer : MonoBehaviour
{

    private static ResultLayer m_Inst;
    public static ResultLayer Instance
    {
        get { return m_Inst; }
    }

    public Text m_TxtResut;
    public Button m_BtnRestart;

    public ResultLayer()
    {
        if (null == m_Inst)
            m_Inst = this;
        else
            Debug.LogError($"{GetType().Name}: multi Inst");
    }

    private void OnDestroy()
    {
        if (this == m_Inst) m_Inst = null;
    }

    private void OnDisable()
    {
        m_BtnRestart.onClick.RemoveAllListeners();
    }

    public void SetResult(string msg)
    {
        m_TxtResut.text = msg;
    }

}
