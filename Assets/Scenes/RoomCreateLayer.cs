﻿using UnityEngine;
using UnityEngine.UI;


public class RoomCreateLayer : MonoBehaviour
{
    private static RoomCreateLayer m_Inst;
    public static RoomCreateLayer Instance
    {
        get { return m_Inst; }
    }

    public Button m_BtnClose;

    public Text m_TxtIp;
    public Button m_BtnServer;

    public InputField m_IptJoinKey;
    public Button m_BtnStart;

    public Text m_TxtTips;
    public Text m_TxtTips2;

    public RoomCreateLayer()
    {
        if (null == m_Inst)
            m_Inst = this;
        else
            Debug.LogError($"{GetType().Name}: multi Inst");
    }

    void Start()
    {
        m_BtnClose.onClick.AddListener(OnClick_Close);
        m_BtnServer.onClick.AddListener(OnClick_Server);
        m_BtnStart.onClick.AddListener(OnClick_Start);
    }

    void OnDestroy()
    {
        if (m_Inst == this) m_Inst = null;
        RegistMsg(false);
    }

    void OnEnable()
    {
        RegistMsg(true);

        m_TxtIp.text = SocketHelper.GetIpV4Address();
        if (null == NetManager.Instance.ServerConn || !NetManager.Instance.ServerConn.IsStarted)
        {
            m_BtnServer.GetComponentInChildren<Text>().text = "启动主机";
        }
        else
        {
            m_BtnServer.GetComponentInChildren<Text>().text = "关闭主机";
        }
        UpdateConnectedClient();
    }

    private void OnDisable()
    {
        RegistMsg(false);
    }

    private void OnClick_Server()
    {
        if (null == NetManager.Instance.ServerConn)
        {
            var serverConn = NetManager.Instance.CreateServerConn();
            serverConn.StartServer();
            m_BtnServer.GetComponentInChildren<Text>().text = "关闭主机";
        }
        else
        {
            NetManager.Instance.CloseServer();
            m_BtnServer.GetComponentInChildren<Text>().text = "启动主机";
        }
    }

    private void OnClick_Close()
    {
        this.gameObject.SetActive(false);
    }

    private void OnClick_Start()
    {
        var gameKey = m_IptJoinKey.text;
        if (!int.TryParse(gameKey, out var v) || 4 != gameKey.Length)
        {
            m_TxtTips2.text = "密令为4位数字";
            return;
        }

        if (null == NetManager.Instance.ServerConn)
        {
            m_TxtTips2.text = "未启动主机";
            return;
        }

        m_TxtTips2.text = "";
        GoBangController_Tcp.Instance.m_GameKey = gameKey;
        GoBangController_Tcp.Instance.m_SelfIsServer = true;
        GoBangTest.Instance.m_Controller = GoBangController_Tcp.Instance;
        this.gameObject.SetActive(false);
        StartLayer.Instance.gameObject.SetActive(false);
        GoBangTest.Instance.gameObject.SetActive(true);
    }

    private void UpdateConnectedClient()
    {
        var acceptConn = NetManager.Instance.AcceptConn;
        if (null != acceptConn && acceptConn.IsConnected)
        {
            m_TxtTips.text = acceptConn.AddressInfo;
        }
        else
        {
            m_TxtTips.text = "暂无";
        }
    }

    //******************** message

    private void RegistMsg(bool b)
    {
        if (b)
        {
            MsgDispatch.Instance.AddReceiver("Server.Accept", OnMsgServer_Accept);
            MsgDispatch.Instance.AddReceiver("Server.ClientDisconnect", OnMsgServer_ClientDisconnect);
        }
        else
        {
            MsgDispatch.Instance.RemoveReceivers(this);
        }
    }

    public void OnMsgServer_Accept(string msg, object data)
    {
        UpdateConnectedClient();
    }

    public void OnMsgServer_ClientDisconnect(string msg, object data)
    {
        UpdateConnectedClient();
    }

    //********************

}
