﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using LitJson;
using System;

public class GoBangTest : MonoBehaviour
    , IPointerClickHandler
{

    private static GoBangTest m_Inst;
    public static GoBangTest Instance
    {
        get { return m_Inst; }
    }

    public Button m_BtnClose;

    public Text m_TxtServerName;
    public Text m_TxtServerTurn;
    public Text m_TxtClientName;
    public Text m_TxtClientTurn;

    public Text m_TxtCDTime;
    public Button m_BtnReady;

    public RectTransform m_GridDotTemplate; //棋盘格子分隔点
    public RectTransform m_ChessTemplate; //棋子

    public RectTransform m_BoardTrans; //棋盘
    public RectTransform m_BoardBgTrans; //棋盘背景
    public RectTransform m_ChessDownHintTrans; //落子提示

    public int m_GridNum = 15; //棋盘格子数量
    private int m_GridSize = 35; //棋盘格子像素大小

    public Queue<RectTransform> m_ChessObjPool = new Queue<RectTransform>(); //对象缓冲池

    public int[][] m_GridChess; //棋盘落子数据
    public int m_ChessDownRow = -1; //落子位置
    public int m_ChessDownCol = -1;
    public int m_ChessCount = 0; //已落子数量

    public int m_LastChessDownRow = -1; //上一步落子位置
    public int m_LastChessDownCol = -1;

    public int m_Status = 0; // 0_未开始, 1_进行中, 2_结果展示
    public int m_TurnFlag = 1; //1_黑色, 2_白色
    public float m_LeftTime = 15;

    public int m_MyChessColor = 0; //当前谁下棋不区分颜色

    [NonSerialized]
    public IGoBangController m_Controller;

    public GoBangTest()
    {
        if (null == m_Inst)
            m_Inst = this;
        else
            Debug.LogError($"{GetType().Name}: multi Inst");
    }

    void OnDestroy()
    {
        if (this == m_Inst) m_Inst = null;
        m_Controller.OnDestroy();
    }

    void Start()
    {
        m_BtnClose.onClick.AddListener(OnClick_Close);

        MoveOutChessDownHint();

        m_GridChess = new int[m_GridNum][];
        for (int row = 0; row < m_GridNum; ++row)
        {
            m_GridChess[row] = new int[m_GridNum];
        }

        int boardSize = m_GridNum * m_GridSize;
        m_BoardTrans.sizeDelta = new Vector2(boardSize, boardSize);
        float gridStartPos = boardSize * 0.5f - m_GridSize * 0.5f;

        Vector2 pos = Vector2.zero;
        for (int row = 0; row < m_GridNum; ++row)
        {
            pos.y = gridStartPos - row * m_GridSize;
            for (int col = 0; col < m_GridNum; ++col)
            {
                pos.x = -gridStartPos + col * m_GridSize;

                var gridDot = GameObject.Instantiate(m_GridDotTemplate);
                gridDot.gameObject.SetActive(true);
                gridDot.SetParent(m_BoardBgTrans, false);
                gridDot.anchoredPosition = pos;
            }
        }
    }

    void OnEnable()
    {
        m_Controller.OnEnable(this);
    }

    void OnDisable()
    {
        m_Controller.OnDisable();
    }

    private void OnClick_Close()
    {
        if (1 == m_Status)
            return;
        this.gameObject.SetActive(false);
        StartLayer.Instance.gameObject.SetActive(true);
    }

    //重新开始
    private void OnClick_Restart()
    {
        ResultLayer.Instance.gameObject.SetActive(false);
        WaitStart();
    }

    public void WaitStart()
    {
        RecycleAllChessObj();
        ResetGridChess();

        MoveOutChessDownHint();
        m_ChessCount = 0;
        m_LastChessDownRow = -1;
        m_LastChessDownCol = -1;

        m_Status = 0;
        m_TurnFlag = 1;
        m_LeftTime = 15;

        m_Controller.OnWaitStart();
    }

    private void RecycleAllChessObj()
    {
        for (int i = 0; i < m_BoardTrans.childCount; ++i)
        {
            var childTrans = m_BoardTrans.GetChild(i);
            if (childTrans.name == "BoardBg" || childTrans.name == "ChessDownHint")
                continue;

            childTrans.gameObject.SetActive(false);
            m_ChessObjPool.Enqueue((RectTransform)childTrans);
        }
    }

    private void ResetGridChess()
    {
        for (int row = 0; row < m_GridNum; ++row)
        {
            for (int col = 0; col < m_GridNum; ++col)
            {
                m_GridChess[row][col] = 0;
            }
        }
    }

    void Update()
    {
        if (null == m_Controller) return;

        switch (m_Status)
        {
        case 1:
        {
            float leftTime = m_LeftTime - Time.deltaTime;
            m_LeftTime = leftTime;

            int turnFlag = m_TurnFlag;
            m_Controller.OnUpdate();
            if (leftTime <= 0)
            {
                leftTime = 0;
                m_Controller.OnLeftTimeUp();
                //m_LeftTime = 15; //到时间了, 不动
            }

            m_TxtCDTime.text = $"{Mathf.Ceil(leftTime)}s";
        }
        break;

        }
    }

    //落子位置指示移到屏幕外
    public void MoveOutChessDownHint()
    {
        m_ChessDownRow = -1;
        m_ChessDownCol = -1;
        m_ChessDownHintTrans.anchoredPosition = new Vector2(-1200, -1200);
    }

    public bool IsMyTurn()
    {
        return 0 == m_MyChessColor || m_MyChessColor == m_TurnFlag;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (1 != m_Status) return;
        if (!IsMyTurn()) return;

        RectTransformUtility.ScreenPointToLocalPointInRectangle(m_BoardBgTrans, eventData.position, eventData.pressEventCamera, out var localPos);
        int boardSize = m_GridNum * m_GridSize;
        float boardHalfSize = boardSize * 0.5f;
        localPos.x += boardHalfSize;
        localPos.y = boardHalfSize - localPos.y;
        int row = Mathf.FloorToInt(localPos.y / m_GridSize);
        int col = Mathf.FloorToInt(localPos.x / m_GridSize);

        if (row < 0 || row >= m_GridNum || col < 0 || col >= m_GridNum)
        {
            Debug.LogWarning($"exceed row or col: {row}, {col}");
            MoveOutChessDownHint();
            return;
        }

        if (row != m_ChessDownRow || col != m_ChessDownCol)
        {
            float gridStartPos = boardHalfSize - m_GridSize * 0.5f;
            m_ChessDownRow = row;
            m_ChessDownCol = col;
            m_ChessDownHintTrans.anchoredPosition = new Vector2(-gridStartPos + col * m_GridSize, gridStartPos - row * m_GridSize);
        }
        else //落子
        {
            if (0 != m_GridChess[row][col])
            {
                Debug.LogWarning($"have chess: {row}, {col}");
                return;
            }

            m_Controller.OnDownChess(m_TurnFlag, row, col);
            MoveOutChessDownHint();
            DownChess(row, col); //不等服务器通知, 直接落子
        }
    }

    public void DownChess(int row, int col)
    {
        if (0 != m_GridChess[row][col])
        {
            Debug.LogWarning($"DownChess: have chess: {row}, {col}");
            return;
        }

        m_LastChessDownRow = row;
        m_LastChessDownCol = col;

        AddChessGameObject(row, col, m_TurnFlag);
        
        m_ChessCount++;
        if (m_ChessCount >= m_GridNum * m_GridNum) //格子满了, 平局
        {
            m_Status = 2;
            ResultLayer.Instance.m_BtnRestart.onClick.AddListener(OnClick_Restart);
            ResultLayer.Instance.gameObject.SetActive(true);
            ResultLayer.Instance.SetResult("平局");
            return;
        }

        if (CheckIsWin(row, col))
            return;

        m_TurnFlag = 3 - m_TurnFlag;
        UpdateTurnHint();

        //落子后重新计时
        m_LeftTime = 15;
        m_Controller.OnNextTurn();
    }

    public void UpdateTurnHint()
    {
        if (1 == m_TurnFlag)
        {
            m_TxtServerTurn.enabled = true;
            m_TxtClientTurn.enabled = false;
        }
        else
        {
            m_TxtServerTurn.enabled = false;
            m_TxtClientTurn.enabled = true;
        }
    }

    public void AddChessGameObject(int row, int col, int chessColor)
    {
        int boardSize = m_GridNum * m_GridSize;
        float boardHalfSize = boardSize * 0.5f;

        RectTransform chessTrans = null;
        if (m_ChessObjPool.Count > 0)
            chessTrans = m_ChessObjPool.Dequeue();
        else
            chessTrans = GameObject.Instantiate(m_ChessTemplate);

        chessTrans.name = $"chess_{row}_{col}";
        chessTrans.gameObject.SetActive(true);
        chessTrans.SetParent(m_BoardTrans, false);
        float gridStartPos = boardHalfSize - m_GridSize * 0.5f;
        chessTrans.anchoredPosition = new Vector2(-gridStartPos + col * m_GridSize, gridStartPos - row * m_GridSize);
        m_GridChess[row][col] = m_TurnFlag;
        if (1 == chessColor)
        {
            chessTrans.GetComponent<Graphic>().color = Color.black;
        }
        else if (2 == chessColor)
        {
            chessTrans.GetComponent<Graphic>().color = Color.white;
        }
    }

    //检查是否有人赢了
    private bool CheckIsWin(int row, int col)
    {
        //检查水平, 垂直, 对角1, 对角2, 是否有5个子连在一起的
        if (CheckSameChessColorNum(m_TurnFlag, row, col, 1, 0, out var emptyBreakNum) >= 4 //水平
            || CheckSameChessColorNum(m_TurnFlag, row, col, 0, 1, out emptyBreakNum) >= 4 //垂直
            || CheckSameChessColorNum(m_TurnFlag, row, col, 1, 1, out emptyBreakNum) >= 4 //对角1
            || CheckSameChessColorNum(m_TurnFlag, row, col, -1, 1, out emptyBreakNum) >= 4 //对角2
        )
        {
            m_Status = 2;
            Debug.Log($"{m_TurnFlag} win !!!");
            ResultLayer.Instance.m_BtnRestart.onClick.AddListener(OnClick_Restart);
            ResultLayer.Instance.gameObject.SetActive(true);
            if (1 == m_TurnFlag)
                ResultLayer.Instance.SetResult("黑棋胜");
            else
                ResultLayer.Instance.SetResult("白棋胜");
            return true;
        }

        return false;
    }

    ///检查某个方向连续同色数量
    public int CheckSameChessColorNum(int chessColor, int rowStart, int colStart, int xDir, int yDir, out int emptyBreakNum)
    {
        emptyBreakNum = 0; //正向, 反向因为没有子而结束的次数
        int sameColorNum = 0; //连续同色数量
        //正向
        for (int i = 1; i <= 4; ++i)
        {
            int row = rowStart + yDir * i;
            if (row < 0 || row >= m_GridNum) continue;
            int col = colStart + xDir * i;
            if (col < 0 || col >= m_GridNum) continue;

            if (m_GridChess[row][col] == chessColor)
            {
                sameColorNum++;
                if (sameColorNum >= 4)
                    return sameColorNum;
            }
            else
            {
                if (0 == m_GridChess[row][col]) //遇到没有落子的grid结束
                    emptyBreakNum++;

                break;
            }
        }

        //反向
        for (int i = -1; i >= -4; --i)
        {
            int row = rowStart + yDir * i;
            if (row < 0 || row >= m_GridNum) continue;
            int col = colStart + xDir * i;
            if (col < 0 || col >= m_GridNum) continue;

            if (m_GridChess[row][col] == chessColor)
            {
                sameColorNum++;
                if (sameColorNum >= 4)
                    return sameColorNum;
            }
            else
            {
                if (0 == m_GridChess[row][col]) //遇到没有落子的grid结束
                    emptyBreakNum++;

                break;
            }
        }

        return sameColorNum;
    }

}
