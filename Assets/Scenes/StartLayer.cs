﻿using UnityEngine;
using UnityEngine.UI;


public class StartLayer : MonoBehaviour
{
    private static StartLayer m_Inst;
    public static StartLayer Instance
    {
        get { return m_Inst; }
    }

    public Button m_BtnStart;
    public Button m_BtnRobot;
    public Button m_BtnCreate;
    public Button m_BtnJoin;

    public StartLayer()
    {
        if (null == m_Inst)
            m_Inst = this;
        else
            Debug.LogError($"{GetType().Name}: multi Inst");
    }

    void OnDestroy()
    {
        if (this == m_Inst) m_Inst = null;
        RegistMsg(false);
    }

    void Start()
    {
        //m_BtnStart.onClick.AddListener(OnClick_Start);
        m_BtnRobot.onClick.AddListener(OnClick_Robot);
        m_BtnCreate.onClick.AddListener(OnClick_Create);
        m_BtnJoin.onClick.AddListener(OnClick_Join);
    }

    private void OnClick_Robot()
    {
        GoBangTest.Instance.m_Controller = GoBangController_Robot.Instance;
        this.gameObject.SetActive(false);
        GoBangTest.Instance.gameObject.SetActive(true);
    }

    private void OnClick_Create()
    {
        RoomCreateLayer.Instance.gameObject.SetActive(true);
    }

    private void OnClick_Join()
    {
        JoinRoomLayer.Instance.gameObject.SetActive(true);
    }

    //******************** message 

    private void RegistMsg(bool b)
    {
        if (b)
        {
            //MsgDispatch.Instance.AddReceiver("", this);
        }
        else
        {
            //MsgDispatch.Instance.RemoveReceiver("", this);
        }
    }

    public void OnMessage(string msg, object data)
    {
    }

    //********************

}
